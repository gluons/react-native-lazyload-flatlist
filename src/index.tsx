import { debounce } from 'lodash-es';
import React, { useEffect, useState } from 'react';
import {
	FlatList,
	FlatListProps,
	ListRenderItem as NativeListRenderItem
} from 'react-native';
import { ValueOf } from 'type-fest';

import delay from './delay';
import sliceData from './sliceData';

export type ListRenderItem<ItemT> = NativeListRenderItem<ItemT>;
export type ItemLayoutFunc<ItemT> = ValueOf<
	FlatListProps<ItemT>,
	'getItemLayout'
>;

/**
 * Function that receive new data items and return mutated data items.
 */
export type OnLoadMoreFunc<ItemT> =
	| ((moreData: ItemT[]) => (ItemT[] | void) | Promise<void | ItemT[]>)
	| null
	| undefined;

export interface LazyLoadFlatListProps<ItemT> extends FlatListProps<ItemT> {
	itemLimit: number;
	onLoadMore?: OnLoadMoreFunc<ItemT>;
}

type LazyLoadFlatListDefaultProps<ItemT> = Pick<
	LazyLoadFlatListProps<ItemT>,
	'itemLimit' | 'onLoadMore'
>;

const LazyLoadFlatList = function <ItemT = any>(
	props: LazyLoadFlatListProps<ItemT>
) {
	const {
		data,
		itemLimit,
		refreshing,
		onEndReached,
		onLoadMore,
		...restProps
	} = props;
	const [allData, setAllData] = useState<readonly ItemT[]>([]);
	const [currentData, setCurrentData] = useState<ItemT[]>([]);
	const [lastLoadIndex, setLastLoadIndex] = useState(0);
	const [loading, setLoading] = useState(false);

	const callOnLoadMore = async (moreData: ItemT[]): Promise<ItemT[]> => {
		if (!onLoadMore) {
			return moreData;
		}

		setLoading(true);

		try {
			// Normalize result to `Promise` via `Promise.resolve`
			const loadMoreResult = await Promise.resolve(onLoadMore(moreData));

			if (Array.isArray(loadMoreResult)) {
				return loadMoreResult;
			}

			return moreData;
		} catch (_: unknown) {
			return moreData;
		} finally {
			setLoading(false);
		}
	};

	const loadMore = async () => {
		if (allData.length === currentData.length) {
			return;
		}

		setLoading(true);

		await delay(300);

		try {
			const { slicedData, lastIndex } = sliceData(
				allData,
				itemLimit,
				lastLoadIndex
			);

			const finalData = await callOnLoadMore(slicedData);
			const newCurrentData = currentData.concat(finalData);

			setCurrentData(newCurrentData);
			setLastLoadIndex(lastIndex);
		} catch (err: unknown) {
			console.error(err);
		} finally {
			setLoading(false);
		}
	};

	useEffect(() => {
		if (!data || data.length === 0) {
			setAllData([]);
			setCurrentData([]);

			return;
		}

		setAllData(data);
		setCurrentData([]);
		setLastLoadIndex(0);

		const { slicedData, lastIndex } = sliceData(data, itemLimit, 0);

		(async () => {
			const finalData = await callOnLoadMore(slicedData);

			setCurrentData(finalData);
			setLastLoadIndex(lastIndex);
		})();

		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [data, itemLimit]);

	return (
		<FlatList
			{...restProps}
			data={currentData}
			refreshing={loading || refreshing}
			onEndReached={debounce(info => {
				if (onEndReached) {
					onEndReached(info);
				}

				void loadMore();
			}, 500)}
		/>
	);
};

const defaultProps: LazyLoadFlatListDefaultProps<any> = {
	itemLimit: 10,
	onLoadMore: () => {}
};
LazyLoadFlatList.defaultProps = defaultProps;

export default LazyLoadFlatList;
