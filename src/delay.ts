/**
 * Delay.
 *
 * @param duration Duration (ms)
 */
export default async function delay(duration: number): Promise<void> {
	return new Promise(resolve => {
		setTimeout(resolve, duration);
	});
}
