export type SliceDataResult<T> = {
	slicedData: T[];
	lastIndex: number;
};

/**
 * Slice array data within desired limit.
 *
 * @template T A data item type
 * @param allData All data
 * @param limit Limit
 * @param startIndex Start index
 * @returns Sliced data
 */
export default function sliceData<T>(
	allData: T[] | readonly T[],
	limit: number,
	startIndex: number
): SliceDataResult<T> {
	if (!Array.isArray(allData) || allData.length <= limit) {
		return {
			slicedData: allData as T[],
			lastIndex: limit
		};
	}

	const endIndex = startIndex + limit;
	const slicedData = allData.slice(startIndex, endIndex);

	return {
		slicedData,
		lastIndex: endIndex
	};
}
