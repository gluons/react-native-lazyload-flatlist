# React Native Lazyload Flatlist
[![npm (scoped)](https://img.shields.io/npm/v/@gluons/react-native-lazyload-flatlist?style=flat-square)](https://www.npmjs.com/package/@gluons/react-native-lazyload-flatlist)

> Personal usage only. No further support.

Lazy loading [`FlatList`](https://reactnative.dev/docs/flatlist) for React Native.

## Installation

```bash
npm install -S @gluons/react-native-lazyload-flatlist
```

or

```bash
yarn add @gluons/react-native-lazyload-flatlist
```

## Usage

```jsx
import { useState } from 'react';
import LazyLoadFlatList from '@gluons/react-native-lazyload-flatlist';

const MainList = () => {
	const [data, setData] = useState([/* data... */]);
	const [lastLoadedData, setLastLoadedData] = useState([]);

	return (
		<LazyLoadFlatList
			data={data}
			itemLimit={20}
			onLoadMore={moreData => {
				setLastLoadedData(moreData);

				return moreData;
			}}
		/>
	)
};

```

## API

### LazyLoadFlatList

| Property   | Type                                                                 | Default    | Description                                                         |
|------------|----------------------------------------------------------------------|------------|---------------------------------------------------------------------|
| itemLimit  | `number`                                                             | `10`       | Number of item limit per lazy load.                                 |
| onLoadMore | `(moreData: ItemT[]) => (ItemT[] \| void) \| Promise<void \| ItemT[]>)` | `() => {}` | Function that receive new data items and return mutated data items. |

And rest props of [`FlatList`](https://reactnative.dev/docs/flatlist#props).
